package router

import (
	"github.com/akijura/myProject/gateway/internal/controller"
	"github.com/gin-gonic/gin"
)

func NewRouter(router *gin.Engine, ctrl *controller.Controller) *gin.Engine {
	router.GET("/ping", ctrl.PingPong)

	apiGroup := router.Group("/api")
	{
		driver := apiGroup.Group("/driver")
		{
			opt := driver.Group("/opt")
			{
				opt.POST("/send", ctrl.SendOpt)
				opt.POST("/confirm", ctrl.ConfirmOpt)
			}
			register := driver.Group("/register").Use(ctrl.CheckDriver())
			{
				register.POST("/license", ctrl.SaveLicense)
				register.POST("/driver-car-info", ctrl.SaveDriverCarInfo)
				register.POST("/upload", ctrl.UploadFile)
				register.GET("/download", ctrl.DownloadFile)
			}

		}
		admin := apiGroup.Group("/admin")
		{
			country := admin.Group("/country")
			{
				country.POST("/", ctrl.CreateCountry)
				country.GET("/", ctrl.GetAllCountry)
				country.GET("/:id", ctrl.GetCountryByID)
				country.PUT("/", ctrl.UpdateCountry)
				country.DELETE("/:id", ctrl.DeleteCountry)
			}
			city := admin.Group("/city")
			{
				city.POST("/", ctrl.CreateCity)
				city.GET("/", ctrl.GetAllCities)
				city.GET("/get-by-country/:id", ctrl.GetAllCitiesByCountryID)
				city.GET("/:id", ctrl.GetCityByID)
				city.PUT("/", ctrl.UpdateCity)
				city.DELETE("/:id", ctrl.DeleteCity)
			}
			brand := admin.Group("/brand")
			{
				brand.POST("/", ctrl.CreateBrand)
				brand.GET("/", ctrl.GetAllBrands)
				brand.GET("/:id", ctrl.GetBrandByID)
				brand.PUT("/", ctrl.UpdateBrand)
				brand.DELETE("/:id", ctrl.DeleteBrand)
			}
			model := admin.Group("/model")
			{
				model.POST("/", ctrl.CreateModel)
				model.GET("/", ctrl.GetAllModels)
				model.GET("/get-by-brand/:id", ctrl.GetAllModelsByBrandID)
				model.GET("/:id", ctrl.GetModelByID)
				model.PUT("/", ctrl.UpdateModel)
				model.DELETE("/:id", ctrl.DeleteModel)
			}
			carInfo := admin.Group("/car-info")
			{
				carInfo.POST("/", ctrl.CreateCarInfo)
				carInfo.GET("/", ctrl.GetAllCarInfos)
				carInfo.GET("/:id", ctrl.GetCarInfoByID)
				carInfo.GET("/get-by-model/:id", ctrl.GetAllCarInfosByModelID)
				carInfo.PUT("/", ctrl.UpdateCarInfo)
				carInfo.DELETE("/:id", ctrl.DeleteCarInfo)
			}
			user := admin.Group("/user")
			{
				user.POST("/create", ctrl.CreateUser)
			}
		}
	}
	return router
}
