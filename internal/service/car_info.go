package service

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
)

func (c *AdminService) CreateCarInfo(ctx *gin.Context, input domain.CarInfoCreateInput) (*pbAdmin.CreateCarInfoResponse, error) {
	rsp, err := c.repo.Admin.CreateCarInfo(ctx, input)
	return rsp, err
}
func (c *AdminService) GetAllCarInfos(ctx *gin.Context) (*pbAdmin.GetAllCarInfosResponse, error) {
	rsp, err := c.repo.Admin.GetAllCarInfos(ctx)
	return rsp, err
}
func (c *AdminService) GetCarInfoByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCarInfoByIDResponse, error) {
	rsp, err := c.repo.Admin.GetCarInfoByID(ctx, ID)
	return rsp, err
}
func (c *AdminService) GetAllCarInfosByModelID(ctx *gin.Context, countryID int64) (*pbAdmin.GetAllCarInfoByModelResponse, error) {
	rsp, err := c.repo.Admin.GetAllCarInfosByModelID(ctx, countryID)
	return rsp, err
}
func (c *AdminService) UpdateCarInfo(ctx *gin.Context, input domain.CarInfoUpdateInput) (*pbAdmin.UpdateCarInfoResponse, error) {
	rsp, err := c.repo.Admin.UpdateCarInfo(ctx, input)
	return rsp, err
}
func (c *AdminService) DeleteCarInfo(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCarInfoResponse, error) {
	rsp, err := c.repo.Admin.DeleteCarInfo(ctx, ID)
	return rsp, err
}
