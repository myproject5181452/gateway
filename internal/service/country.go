package service

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/repository"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
)

type AdminService struct {
	repo *repository.Repositories
}

func NewAdminService(repo *repository.Repositories) *AdminService {
	return &AdminService{repo: repo}
}
func (c *AdminService) CreateCountry(ctx *gin.Context, input domain.CountryCreateInput) (*pbAdmin.CreateCountryResponse, error) {
	rsp, err := c.repo.Admin.CreateCountry(ctx, input)
	return rsp, err
}
func (c *AdminService) GetAllCountry(ctx *gin.Context) (*pbAdmin.GetAllCountryResponse, error) {
	rsp, err := c.repo.Admin.GetAllCountry(ctx)
	return rsp, err
}

func (c *AdminService) GetCountryByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCountryByIDResponse, error) {
	rsp, err := c.repo.Admin.GetCountryByID(ctx, ID)
	return rsp, err
}

func (c *AdminService) UpdateCountry(ctx *gin.Context, input domain.CountryUpdateInput) (*pbAdmin.UpdateCountryResponse, error) {
	rsp, err := c.repo.Admin.UpdateCountry(ctx, input)
	return rsp, err
}

func (c *AdminService) DeleteCountry(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCountryResponse, error) {
	rsp, err := c.repo.Admin.DeleteCountry(ctx, ID)
	return rsp, err
}
