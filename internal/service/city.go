package service

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
)

func (c *AdminService) CreateCity(ctx *gin.Context, input domain.CityCreateInput) (*pbAdmin.CreateCityResponse, error) {
	rsp, err := c.repo.Admin.CreateCity(ctx, input)
	return rsp, err
}
func (c *AdminService) GetAllCities(ctx *gin.Context) (*pbAdmin.GetAllCitiesResponse, error) {
	rsp, err := c.repo.Admin.GetAllCities(ctx)
	return rsp, err
}

func (c *AdminService) GetAllCitiesByCountryID(ctx *gin.Context, countryID int64) (*pbAdmin.GetAllCitiesByCountryResponse, error) {
	rsp, err := c.repo.Admin.GetAllCitiesByCountryID(ctx, countryID)
	return rsp, err
}

func (c *AdminService) GetCityByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCityByIDResponse, error) {
	rsp, err := c.repo.Admin.GetCityByID(ctx, ID)
	return rsp, err
}

func (c *AdminService) UpdateCity(ctx *gin.Context, input domain.CityUpdateInput) (*pbAdmin.UpdateCityResponse, error) {
	rsp, err := c.repo.Admin.UpdateCity(ctx, input)
	return rsp, err
}

func (c *AdminService) DeleteCity(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCityResponse, error) {
	rsp, err := c.repo.Admin.DeleteCity(ctx, ID)
	return rsp, err
}
