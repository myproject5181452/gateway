package service

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
)

func (c *AdminService) CreateModel(ctx *gin.Context, input domain.ModelCreateInput) (*pbAdmin.CreateModelResponse, error) {
	rsp, err := c.repo.Admin.CreateModel(ctx, input)
	return rsp, err
}
func (c *AdminService) GetAllModels(ctx *gin.Context) (*pbAdmin.GetAllModelsResponse, error) {
	rsp, err := c.repo.Admin.GetAllModels(ctx)
	return rsp, err
}

func (c *AdminService) GetAllModelsByBrandID(ctx *gin.Context, brandID int64) (*pbAdmin.GetAllModelsByBrandResponse, error) {
	rsp, err := c.repo.Admin.GetAllModelsByBrandID(ctx, brandID)
	return rsp, err
}

func (c *AdminService) GetModelByID(ctx *gin.Context, ID int64) (*pbAdmin.GetModelByIDResponse, error) {
	rsp, err := c.repo.Admin.GetModelByID(ctx, ID)
	return rsp, err
}

func (c *AdminService) UpdateModel(ctx *gin.Context, input domain.ModelUpdateInput) (*pbAdmin.UpdateModelResponse, error) {
	rsp, err := c.repo.Admin.UpdateModel(ctx, input)
	return rsp, err
}

func (c *AdminService) DeleteModel(ctx *gin.Context, ID int64) (*pbAdmin.DeleteModelResponse, error) {
	rsp, err := c.repo.Admin.DeleteModel(ctx, ID)
	return rsp, err
}
