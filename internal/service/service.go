package service

import (
	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/repository"
	driverservice "github.com/akijura/myProject/gateway/internal/service/driver_service"
)

type Service struct {
	repo *repository.Repositories
	conf *config.Config

	Driver *driverservice.DriverService
	Admin  *AdminService
}

func NewService(repo *repository.Repositories, conf *config.Config) *Service {
	return &Service{
		repo: repo,
		conf: conf,

		Driver: driverservice.NewDriverService(repo),
		Admin:  NewAdminService(repo),
	}
}
