package service

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
)

func (c *AdminService) CreateUser(ctx *gin.Context, input domain.UserCreateInput) (*pbAdmin.CreateUserResponse, error) {
	rsp, err := c.repo.Admin.CreateUser(ctx, input)
	return rsp, err
}
