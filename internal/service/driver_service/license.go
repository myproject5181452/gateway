package driverservice

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/repository"
	pbDriver "github.com/akijura/myProject/gateway/internal/repository/driver/pb"
	"github.com/gin-gonic/gin"
)

type DriverService struct {
	repo *repository.Repositories
}

func NewDriverService(repo *repository.Repositories) *DriverService {
	return &DriverService{repo: repo}
}
func (c *DriverService) SaveLicense(ctx *gin.Context, input domain.LicenseCreateInput) (*pbDriver.LicenseResponse, error) {
	rsp, err := c.repo.Driver.SaveLicense(ctx, input)
	return rsp, err
}
