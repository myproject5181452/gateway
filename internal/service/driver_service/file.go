package driverservice

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbDriver "github.com/akijura/myProject/gateway/internal/repository/driver/pb"
	"github.com/gin-gonic/gin"
)

func (c *DriverService) UploadFile(ctx *gin.Context, input domain.FileUploadInput) (*pbDriver.StatusCode, error) {
	rsp, err := c.repo.Driver.UploadFile(ctx, input)
	return rsp, err
}

func (c *DriverService) DownloadFile(ctx *gin.Context, fileName string) (string, error) {
	rsp, err := c.repo.Driver.DownloadFile(ctx, fileName)
	return rsp, err
}
