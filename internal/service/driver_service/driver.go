package driverservice

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbDriver "github.com/akijura/myProject/gateway/internal/repository/driver/pb"
	"github.com/gin-gonic/gin"
)

func (c *DriverService) SendOpt(ctx *gin.Context, input domain.OTPData) (*pbDriver.SendOptResponse, error) {
	rsp, err := c.repo.Driver.SendOpt(ctx, input)
	return rsp, err
}
func (c *DriverService) ConfirmOpt(ctx *gin.Context, input domain.VerifyOTP) (*pbDriver.ConfirmOptResponse, error) {
	rsp, err := c.repo.Driver.ConfirmOpt(ctx, input)
	return rsp, err
}
func (c *DriverService) CheckDriver(ctx *gin.Context, token string) (*pbDriver.CheckDriverResponse, error) {
	rsp, err := c.repo.Driver.CheckDriver(ctx, token)
	return rsp, err
}
func (c *DriverService) SaveDriverCarInfo(ctx *gin.Context, input domain.DriverCarInfoCreateInfo) (*pbDriver.DriverCarInfoResponse, error) {
	rsp, err := c.repo.Driver.SaveDriverCarInfo(ctx, input)
	return rsp, err
}
