package service

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
)

func (c *AdminService) CreateBrand(ctx *gin.Context, input domain.BrandCreateInput) (*pbAdmin.CreateBrandResponse, error) {
	rsp, err := c.repo.Admin.CreateBrand(ctx, input)
	return rsp, err
}
func (c *AdminService) GetAllBrands(ctx *gin.Context) (*pbAdmin.GetAllBrandsResponse, error) {
	rsp, err := c.repo.Admin.GetAllBrands(ctx)
	return rsp, err
}

func (c *AdminService) GetBrandByID(ctx *gin.Context, ID int64) (*pbAdmin.GetBrandByIDResponse, error) {
	rsp, err := c.repo.Admin.GetBrandByID(ctx, ID)
	return rsp, err
}

func (c *AdminService) UpdateBrand(ctx *gin.Context, input domain.BrandUpdateInput) (*pbAdmin.UpdateBrandResponse, error) {
	rsp, err := c.repo.Admin.UpdateBrand(ctx, input)
	return rsp, err
}

func (c *AdminService) DeleteBrand(ctx *gin.Context, ID int64) (*pbAdmin.DeleteBrandResponse, error) {
	rsp, err := c.repo.Admin.DeleteBrand(ctx, ID)
	return rsp, err
}
