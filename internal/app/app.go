package app

import (
	"os"
	"os/signal"
	"syscall"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/controller"
	"github.com/akijura/myProject/gateway/internal/repository"
	"github.com/akijura/myProject/gateway/internal/router"
	"github.com/akijura/myProject/gateway/internal/service"
	"github.com/akijura/myProject/gateway/pkg/httpserver"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

var RootPath, _ = os.Getwd()

func Run() {

	zap.L().Info("initializing zap logger")
	logger := setUpLogger()
	defer logger.Sync()
	zap.ReplaceGlobals(logger)

	zap.L().Info("initializing configs")
	cfg := config.NewConfig()

	zap.L().Info("initializing repositories")
	repo := repository.NewRepositories()

	zap.L().Info("initializing services")
	serv := service.NewService(repo, cfg)

	zap.L().Info("initializing controllers")
	ctrl := controller.NewController(serv, cfg)

	zap.L().Info("initializing gin log files")
	infoLog, err := os.OpenFile(RootPath+"/logs/gin/info.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		zap.L().Fatal("cannot open info log file")
	}
	errLog, err := os.OpenFile(RootPath+"/logs/gin/error.log", os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)
	if err != nil {
		zap.L().Fatal("cannot open error log file")
	}
	zap.L().Info("initializing router")
	engine := gin.New()
	engine.Use(controller.GinLogger(logger), gin.Recovery())
	gin.DefaultWriter = infoLog
	gin.DefaultErrorWriter = errLog
	r := router.NewRouter(engine, ctrl)

	zap.L().Info("initializing server")
	httpServer := httpserver.New(r, httpserver.Port(cfg.HTTPServerAddress))
	zap.L().Info("SERVER RUNNING...")
	zap.L().Info("Waiting signal")
	zap.L().Info("Configuring graceful shutdown...")
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, syscall.SIGTERM)

	select {
	case s := <-interrupt:
		zap.L().Info("app - Run - signal: " + s.String())
	case err = <-httpServer.Notify():
		zap.L().Fatal("app - Run - httpServer.Notify: ", zap.Error(err))
	}

	// Graceful shutdown
	zap.L().Info("Shutting down...")
	err = httpServer.Shutdown()
	if err != nil {
		zap.L().Error("app - Run - httpServer.Shutdown:", zap.Error(err))
	}
}
