package errs

import (
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc/status"
)

func ErrorResponse(err error) gin.H {
	status, ok := status.FromError(err)
	if !ok {
		return gin.H{"error": err.Error()}
	}
	return gin.H{
		"status":  status.Code(),
		"message": status.Message(),
		"error":   err.Error(),
	}
}
