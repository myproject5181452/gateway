package controller

import (
	"net/http"
	"strconv"

	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/errs"
	"github.com/gin-gonic/gin"
)

func (c *Controller) CreateCountry(ctx *gin.Context) {
	var req domain.CountryCreateInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.CreateCountry(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) GetAllCountry(ctx *gin.Context) {
	rsp, err := c.s.Admin.GetAllCountry(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) GetCountryByID(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.GetCountryByID(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) UpdateCountry(ctx *gin.Context) {
	var req domain.CountryUpdateInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.UpdateCountry(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) DeleteCountry(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.DeleteCountry(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
