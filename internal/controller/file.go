package controller

import (
	"net/http"
	"os"

	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/errs"
	"github.com/gin-gonic/gin"
)

func (c *Controller) UploadFile(ctx *gin.Context) {
	var req domain.FileUploadInput
	file, header, err := ctx.Request.FormFile("file")
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	req.FileName = header.Filename
	req.Size = int(header.Size)
	req.File = file
	rsp, err := c.s.Driver.UploadFile(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) DownloadFile(ctx *gin.Context) {
	var req domain.FileDownloadInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Driver.DownloadFile(ctx, req.FileName)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.FileAttachment(c.conf.TempLocation+"/"+rsp, rsp)
	os.Remove(c.conf.TempLocation + "/" + rsp)
}
