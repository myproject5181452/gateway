package controller

import (
	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/service"
)

type Controller struct {
	s    *service.Service
	conf *config.Config
}

func NewController(s *service.Service, cfg *config.Config) *Controller {
	return &Controller{s: s, conf: cfg}
}
