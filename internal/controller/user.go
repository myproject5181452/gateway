package controller

import (
	"net/http"

	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/errs"
	"github.com/gin-gonic/gin"
)

func (c *Controller) CreateUser(ctx *gin.Context) {
	var req domain.UserCreateInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.CreateUser(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
