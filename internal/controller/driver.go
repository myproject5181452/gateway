package controller

import (
	"net/http"

	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/errs"
	"github.com/gin-gonic/gin"
)

func (c *Controller) PingPong(ctx *gin.Context) {
	ctx.JSON(http.StatusOK, "pong")
}

func (c *Controller) SendOpt(ctx *gin.Context) {
	var req domain.OTPData
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Driver.SendOpt(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) ConfirmOpt(ctx *gin.Context) {
	var req domain.VerifyOTP
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Driver.ConfirmOpt(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) SaveDriverCarInfo(ctx *gin.Context) {
	var req domain.DriverCarInfoCreateInfo
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Driver.SaveDriverCarInfo(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
