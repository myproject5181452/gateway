package controller

import (
	"net/http"
	"strconv"

	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/errs"
	"github.com/gin-gonic/gin"
)

func (c *Controller) CreateCarInfo(ctx *gin.Context) {
	var req domain.CarInfoCreateInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.CreateCarInfo(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) GetAllCarInfos(ctx *gin.Context) {
	rsp, err := c.s.Admin.GetAllCarInfos(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
func (c *Controller) GetCarInfoByID(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.GetCarInfoByID(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
func (c *Controller) GetAllCarInfosByModelID(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.GetAllCarInfosByModelID(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
func (c *Controller) UpdateCarInfo(ctx *gin.Context) {
	var req domain.CarInfoUpdateInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.UpdateCarInfo(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
func (c *Controller) DeleteCarInfo(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.DeleteCarInfo(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
