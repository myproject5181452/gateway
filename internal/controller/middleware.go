package controller

import (
	"errors"
	"log"
	"net/http"
	"strings"

	"github.com/akijura/myProject/gateway/internal/errs"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func (c *Controller) CheckDriver() gin.HandlerFunc {
	return func(ctx *gin.Context) {
		token, ok := bearerToken(ctx.Request)
		log.Println(token)
		if !ok {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": errs.ErrorResponse(errors.New("user logged out or not authorized in first if"))})
			return
		}
		driverData, err := c.s.Driver.CheckDriver(ctx, token)
		log.Println(driverData)
		if err != nil {
			ctx.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{"error": errs.ErrorResponse(err)})
			return
		}
		ctx.Set("driver_id", driverData.GetId())
		ctx.Set("role_id", driverData.GetRole())
		ctx.Next()
	}
}
func bearerToken(r *http.Request) (string, bool) {
	const prefix = "Bearer "
	var token string

	header := r.Header.Get("Authorization")

	if header == "" {
		return "", false
	}
	tokenArr := strings.Split(header, " ")
	token = tokenArr[1]
	log.Println("inisde berarToken")
	log.Println(token)
	if token == "" {
		return "", false
	}
	if len(header) > len(prefix) && strings.EqualFold(header[:len(prefix)], prefix) {
		return header[len(prefix):], true
	}
	return "", false
}

func GinLogger(logger *zap.Logger) gin.HandlerFunc {
	return func(c *gin.Context) {
		// Create a child logger for each request
		reqLogger := logger.With(
			zap.String("method", c.Request.Method),
			zap.String("path", c.Request.URL.Path),
			zap.String("ip", c.ClientIP()),
		)

		// Log the request
		reqLogger.Info("Request received")

		// Process the request
		c.Next()
	}
}
