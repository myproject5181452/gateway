package controller

import (
	"net/http"
	"strconv"

	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/errs"
	"github.com/gin-gonic/gin"
)

func (c *Controller) CreateCity(ctx *gin.Context) {
	var req domain.CityCreateInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.CreateCity(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) GetAllCities(ctx *gin.Context) {
	rsp, err := c.s.Admin.GetAllCities(ctx)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) GetAllCitiesByCountryID(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.GetAllCitiesByCountryID(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) GetCityByID(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.GetCityByID(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) UpdateCity(ctx *gin.Context) {
	var req domain.CityUpdateInput
	if err := ctx.ShouldBindJSON(&req); err != nil {
		ctx.JSON(http.StatusBadRequest, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.UpdateCity(ctx, req)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}

func (c *Controller) DeleteCity(ctx *gin.Context) {
	id := ctx.Param("id")
	if id == "" {
		ctx.JSON(http.StatusBadRequest, "Please fill all fields")
		return
	}
	idInt, err := strconv.ParseInt(id, 10, 64)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	rsp, err := c.s.Admin.DeleteCity(ctx, idInt)
	if err != nil {
		ctx.JSON(http.StatusInternalServerError, errs.ErrorResponse(err))
		return
	}
	ctx.JSON(http.StatusOK, rsp)
}
