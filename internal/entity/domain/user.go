package domain

type UserCreateInput struct {
	Login       string `json:"login" binding:"required"`
	Password    string `json:"password" binding:"required"`
	Name        string `json:"name" binding:"required"`
	PhoneNumber string `json:"phone_number" binding:"required"`
	Role        int    `json:"role" binding:"required"`
	Status      int    `json:"status" binding:"required"`
}
