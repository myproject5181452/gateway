package domain

type LicenseCreateInput struct {
	DriverID      int64  `json:"driver_id" binding:"required"`
	CountryID     int64  `json:"country_id" binding:"required"`
	LicenseNumber string `json:"license_number" binding:"required"`
	Date          string `json:"date" binding:"required"`
	CityID        int64  `json:"city_id" binding:"required"`
	LastName      string `json:"last_name" binding:"required"`
	FirstName     string `json:"first_name" binding:"required"`
}
