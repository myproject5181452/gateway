package domain

type CountryCreateInput struct {
	Name      string `json:"name" binding:"required"`
	ShortName string `json:"short_name" binding:"required"`
	Status    int    `json:"status" binding:"required"`
	AuthorID  int    `json:"author_id" binding:"required"`
}

type CountryUpdateInput struct {
	ID        int64  `json:"id" binding:"required"`
	Name      string `json:"name" binding:"required"`
	ShortName string `json:"short_name" binding:"required"`
	Status    int    `json:"status" binding:"required"`
	AuthorID  int    `json:"author_id" binding:"required"`
}

type GetCountryByID struct {
	ID int64 `json:"id" binding:"required"`
}
