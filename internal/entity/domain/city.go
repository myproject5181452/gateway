package domain

type CityCreateInput struct {
	Name      string `json:"name" binding:"required"`
	Status    int    `json:"status" binding:"required"`
	CountryID int64  `json:"country_id" binding:"required"`
	AuthorID  int64  `json:"author_id" binding:"required"`
}
type CityUpdateInput struct {
	ID        int64  `json:"id" binding:"required"`
	Name      string `json:"name" binding:"required"`
	CountryID int64  `json:"country_id" binding:"required"`
	Status    int    `json:"status" binding:"required"`
	AuthorID  int    `json:"author_id" binding:"required"`
}
