package domain

type DriverCarInfoCreateInfo struct {
	DriverID     int64  `json:"driver_id" binding:"required"`
	ModelID      int64  `json:"model_id" binding:"required"`
	Color        int32  `json:"color" binding:"required"`
	CarNumber    string `json:"car_number" binding:"required"`
	CarTexNumber string `json:"car_tex_number" binding:"required"`
	Date         string `json:"date" binding:"required"`
}
