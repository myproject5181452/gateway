package domain

import "mime/multipart"

type FileUploadInput struct {
	FileName string         `json:"file_name"`
	Size     int            `json:"size"`
	File     multipart.File `json:"file"`
}
type FileDownloadInput struct {
	FileName string `json:"file_name" binding:"required"`
}
