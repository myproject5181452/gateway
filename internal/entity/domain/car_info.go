package domain

type CarInfoCreateInput struct {
	ModelId      int64  `json:"model_id" binding:"required"`
	Length       int32  `json:"length" binding:"required"`
	Width        int32  `json:"width" binding:"required"`
	Height       int32  `json:"height" binding:"required"`
	Volume       int32  `json:"volume" binding:"required"`
	LoadCapacity int32  `json:"load_capacity" binding:"required"`
	Close        int32  `json:"close" binding:"required"`
	Description  string `json:"decription" binding:"required"`
	AuthorID     int64  `json:"author_id" binding:"required"`
}
type CarInfoUpdateInput struct {
	ID           int64  `json:"id" binding:"required"`
	ModelID      int64  `json:"model_id" binding:"required"`
	Length       int32  `json:"length" binding:"required"`
	Width        int32  `json:"width" binding:"required"`
	Height       int32  `json:"height" binding:"required"`
	Volume       int32  `json:"volume" binding:"required"`
	LoadCapacity int32  `json:"load_capacity" binding:"required"`
	Close        int32  `json:"close" binding:"required"`
	Description  string `json:"decription" binding:"required"`
	AuthorID     int64  `json:"author_id" binding:"required"`
}
