package domain

type OTPData struct {
	PhoneNumber string `json:"phone_number" binding:"required"`
}

type VerifyOTP struct {
	VerificationCode string `json:"verification_code" binding:"required"`
	OTPData
}
