package config

import (
	"os"
)

type Config struct {
	HTTPServerAddress string
	RPCDriverService  string
	RPCAdminService   string
	GinMode           string

	TempLocation string
}

func NewConfig() *Config {
	cfg := &Config{}
	cfg.GinMode = os.Getenv("GIN_MODE")

	cfg.HTTPServerAddress = os.Getenv("HTTP_SERVER_ADDRESS")
	cfg.RPCDriverService = os.Getenv("RPC_DRIVER_SERVICE")
	cfg.RPCAdminService = os.Getenv("RPC_ADMIN_SERVICE")

	cfg.TempLocation = os.Getenv("TEMP_LOCATION")
	return cfg
}
