package driver

import (
	"context"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbDriver "github.com/akijura/myProject/gateway/internal/repository/driver/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Driver struct{}

func NewDriver() *Driver {
	return &Driver{}
}

func (d *Driver) SendOpt(ctx *gin.Context, input domain.OTPData) (*pbDriver.SendOptResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCDriverService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbDriver.NewDriverSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.SendOpt(cx, &pbDriver.SendOptRequest{
		PhoneNumber: input.PhoneNumber,
		Lang:        ctx.GetHeader("lang"),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Driver) ConfirmOpt(ctx *gin.Context, input domain.VerifyOTP) (*pbDriver.ConfirmOptResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCDriverService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbDriver.NewDriverSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.ConfirmOpt(cx, &pbDriver.ConfirmOptRequest{
		PhoneNumber:      input.PhoneNumber,
		VerificationCode: input.VerificationCode,
		Lang:             ctx.GetHeader("lang"),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
func (d *Driver) CheckDriver(ctx *gin.Context, token string) (*pbDriver.CheckDriverResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCDriverService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbDriver.NewDriverSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.CheckDriver(cx, &pbDriver.CheckDriverRequest{
		Token: token,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Driver) SaveDriverCarInfo(ctx *gin.Context, input domain.DriverCarInfoCreateInfo) (*pbDriver.DriverCarInfoResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCDriverService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbDriver.NewDriverSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.SaveDriverCarInfo(cx, &pbDriver.DriverCarInfoRequest{
		DriverId:     input.DriverID,
		ModelId:      input.ModelID,
		Color:        input.Color,
		CarNumber:    input.CarNumber,
		CarTexNumber: input.CarTexNumber,
		Date:         input.Date,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
