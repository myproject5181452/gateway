package driver

import (
	"context"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbDriver "github.com/akijura/myProject/gateway/internal/repository/driver/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (d *Driver) SaveLicense(ctx *gin.Context, input domain.LicenseCreateInput) (*pbDriver.LicenseResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCDriverService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbDriver.NewDriverSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.SaveLicense(cx, &pbDriver.LicenseRequest{
		DriverId:      input.CountryID,
		CountryId:     input.CountryID,
		LicenseNumber: input.LicenseNumber,
		Date:          input.Date,
		CityId:        input.CityID,
		LastName:      input.LastName,
		FirstName:     input.FirstName,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
