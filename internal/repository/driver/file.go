package driver

import (
	"context"
	"errors"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"strconv"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbDriver "github.com/akijura/myProject/gateway/internal/repository/driver/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"google.golang.org/grpc/metadata"
)

func (d *Driver) UploadFile(ctx *gin.Context, input domain.FileUploadInput) (*pbDriver.StatusCode, error) {
	md := metadata.Pairs(
		"filename", filepath.Base(input.FileName),
		"size", fmt.Sprint(input.Size))
	conn, err := grpc.Dial(config.NewConfig().RPCDriverService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()

	c := pbDriver.NewDriverSerClient(conn)
	cx := metadata.NewOutgoingContext(context.Background(), md)
	stream, err := c.UploadFile(cx)
	if err != nil {
		return nil, err
	}
	buf := make([]byte, 4*1024)
	for {
		// put as many bytes as `chunkSize` into the buf array.
		n, err := input.File.Read(buf)
		if err != nil {
			if err == io.EOF {
				break
			}
			return nil, err
		}

		err = stream.Send(&pbDriver.FileUploadRequest{
			Chunk: buf[:n],
		})
		if err != nil {
			return nil, err
		}
	}
	status, err := stream.CloseAndRecv()
	if err != nil {
		return nil, err
	}
	return &status.Code, nil
}

func (d *Driver) DownloadFile(ctx *gin.Context, fileName string) (name string, err error) {
	conn, err := grpc.Dial(config.NewConfig().RPCDriverService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return "", err
	}

	defer conn.Close()
	c := pbDriver.NewDriverSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	stream, err := c.DownloadFile(cx, &pbDriver.FileDownloadRequest{
		FileName: fileName,
	})
	if err != nil {
		return "", err
	}
	header, err := stream.Header()
	if err != nil {
		return "", err
	}
	filename := header["filename"][0]
	filesize, err := strconv.Atoi(header["size"][0])
	if err != nil {
		return "", err
	}
	size := 0
	file, err := os.OpenFile(config.NewConfig().TempLocation+"/"+filename, os.O_CREATE|os.O_RDWR, 0644)
	if err != nil {
		return "", err
	}
	defer file.Close()
	for {
		data, err := stream.Recv()
		if err != nil {
			if err == io.EOF {
				break
			}
		}
		size += len(data.FileChunk)
		file.Write(data.FileChunk)
	}
	if size != filesize {
		return "", errors.New("recv size not matched data size")
	}
	return filename, nil
}
