package repository

import (
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	"github.com/akijura/myProject/gateway/internal/repository/admin"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/akijura/myProject/gateway/internal/repository/driver"
	pbDriver "github.com/akijura/myProject/gateway/internal/repository/driver/pb"
	"github.com/gin-gonic/gin"
)

type Driver interface {
	SendOpt(ctx *gin.Context, input domain.OTPData) (*pbDriver.SendOptResponse, error)
	ConfirmOpt(ctx *gin.Context, input domain.VerifyOTP) (*pbDriver.ConfirmOptResponse, error)

	SaveLicense(ctx *gin.Context, input domain.LicenseCreateInput) (*pbDriver.LicenseResponse, error)
	SaveDriverCarInfo(ctx *gin.Context, input domain.DriverCarInfoCreateInfo) (*pbDriver.DriverCarInfoResponse, error)

	CheckDriver(ctx *gin.Context, token string) (*pbDriver.CheckDriverResponse, error)

	UploadFile(ctx *gin.Context, input domain.FileUploadInput) (*pbDriver.StatusCode, error)
	DownloadFile(ctx *gin.Context, fileName string) (string, error)
}

type Admin interface {
	CreateUser(ctx *gin.Context, input domain.UserCreateInput) (*pbAdmin.CreateUserResponse, error)

	CreateCountry(ctx *gin.Context, input domain.CountryCreateInput) (*pbAdmin.CreateCountryResponse, error)
	GetAllCountry(ctx *gin.Context) (*pbAdmin.GetAllCountryResponse, error)
	GetCountryByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCountryByIDResponse, error)
	UpdateCountry(ctx *gin.Context, input domain.CountryUpdateInput) (*pbAdmin.UpdateCountryResponse, error)
	DeleteCountry(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCountryResponse, error)

	CreateCity(ctx *gin.Context, input domain.CityCreateInput) (*pbAdmin.CreateCityResponse, error)
	GetAllCities(ctx *gin.Context) (*pbAdmin.GetAllCitiesResponse, error)
	GetAllCitiesByCountryID(ctx *gin.Context, countryID int64) (*pbAdmin.GetAllCitiesByCountryResponse, error)
	GetCityByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCityByIDResponse, error)
	UpdateCity(ctx *gin.Context, input domain.CityUpdateInput) (*pbAdmin.UpdateCityResponse, error)
	DeleteCity(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCityResponse, error)

	CreateBrand(ctx *gin.Context, input domain.BrandCreateInput) (*pbAdmin.CreateBrandResponse, error)
	GetAllBrands(ctx *gin.Context) (*pbAdmin.GetAllBrandsResponse, error)
	GetBrandByID(ctx *gin.Context, ID int64) (*pbAdmin.GetBrandByIDResponse, error)
	UpdateBrand(ctx *gin.Context, input domain.BrandUpdateInput) (*pbAdmin.UpdateBrandResponse, error)
	DeleteBrand(ctx *gin.Context, ID int64) (*pbAdmin.DeleteBrandResponse, error)

	CreateModel(ctx *gin.Context, input domain.ModelCreateInput) (*pbAdmin.CreateModelResponse, error)
	GetAllModels(ctx *gin.Context) (*pbAdmin.GetAllModelsResponse, error)
	GetAllModelsByBrandID(ctx *gin.Context, brandID int64) (*pbAdmin.GetAllModelsByBrandResponse, error)
	GetModelByID(ctx *gin.Context, ID int64) (*pbAdmin.GetModelByIDResponse, error)
	UpdateModel(ctx *gin.Context, input domain.ModelUpdateInput) (*pbAdmin.UpdateModelResponse, error)
	DeleteModel(ctx *gin.Context, ID int64) (*pbAdmin.DeleteModelResponse, error)

	CreateCarInfo(ctx *gin.Context, input domain.CarInfoCreateInput) (*pbAdmin.CreateCarInfoResponse, error)
	GetAllCarInfos(ctx *gin.Context) (*pbAdmin.GetAllCarInfosResponse, error)
	GetCarInfoByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCarInfoByIDResponse, error)
	GetAllCarInfosByModelID(ctx *gin.Context, ID int64) (*pbAdmin.GetAllCarInfoByModelResponse, error)
	UpdateCarInfo(ctx *gin.Context, input domain.CarInfoUpdateInput) (*pbAdmin.UpdateCarInfoResponse, error)
	DeleteCarInfo(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCarInfoResponse, error)
}

type Repositories struct {
	Driver
	Admin
}

func NewRepositories() *Repositories {
	return &Repositories{
		driver.NewDriver(),
		admin.NewAdmin(),
	}
}
