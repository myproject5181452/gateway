package admin

import (
	"context"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (d *Admin) CreateBrand(ctx *gin.Context, input domain.BrandCreateInput) (*pbAdmin.CreateBrandResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.CreateBrand(cx, &pbAdmin.CreateBrandRequest{
		Name:     input.Name,
		Status:   int32(input.Status),
		AuthorId: input.AuthorID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetAllBrands(ctx *gin.Context) (*pbAdmin.GetAllBrandsResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllBrands(cx, &pbAdmin.GetAllBrandsRequest{
		Status: 1,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetBrandByID(ctx *gin.Context, ID int64) (*pbAdmin.GetBrandByIDResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetBrandByID(cx, &pbAdmin.GetBrandByIDRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) UpdateBrand(ctx *gin.Context, input domain.BrandUpdateInput) (*pbAdmin.UpdateBrandResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.UpdateBrand(cx, &pbAdmin.UpdateBrandRequest{
		ID:       input.ID,
		Name:     input.Name,
		Status:   int32(input.Status),
		AuthorId: int64(input.AuthorID),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) DeleteBrand(ctx *gin.Context, ID int64) (*pbAdmin.DeleteBrandResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.DeleteBrand(cx, &pbAdmin.DeleteBrandRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
