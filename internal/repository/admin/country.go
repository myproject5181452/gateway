package admin

import (
	"context"
	"fmt"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type Admin struct{}

func NewAdmin() *Admin {
	return &Admin{}
}
func (d *Admin) CreateCountry(ctx *gin.Context, input domain.CountryCreateInput) (*pbAdmin.CreateCountryResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.CreateCountry(cx, &pbAdmin.CreateCountryRequest{
		Name:      input.Name,
		ShortName: input.ShortName,
		Status:    fmt.Sprint(input.Status),
		AuthorId:  int64(input.AuthorID),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetAllCountry(ctx *gin.Context) (*pbAdmin.GetAllCountryResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllCountry(cx, &pbAdmin.GetAllCountryRequest{
		Status: 1,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
func (d *Admin) GetCountryByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCountryByIDResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetCountryByID(cx, &pbAdmin.GetCountryByIDRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
func (d *Admin) UpdateCountry(ctx *gin.Context, input domain.CountryUpdateInput) (*pbAdmin.UpdateCountryResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.UpdateCountry(cx, &pbAdmin.UpdateCountryRequest{
		ID:        input.ID,
		Name:      input.Name,
		ShortName: input.ShortName,
		Status:    fmt.Sprint(input.Status),
		AuthorId:  int64(input.AuthorID),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) DeleteCountry(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCountryResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.DeleteCountry(cx, &pbAdmin.DeleteCountryRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
