package admin

import (
	"context"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (d *Admin) CreateCarInfo(ctx *gin.Context, input domain.CarInfoCreateInput) (*pbAdmin.CreateCarInfoResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.CreateCarInfo(cx, &pbAdmin.CreateCarInfoRequest{
		ModelId:      input.ModelId,
		Length:       input.Length,
		Width:        input.Width,
		Height:       input.Height,
		Volume:       input.Volume,
		LoadCapacity: input.LoadCapacity,
		Close:        input.Close,
		Description:  input.Description,
		AuthorId:     input.AuthorID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetAllCarInfos(ctx *gin.Context) (*pbAdmin.GetAllCarInfosResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllCarInfos(cx, &pbAdmin.GetAllCarInfosRequest{
		Status: 1,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
func (d *Admin) GetCarInfoByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCarInfoByIDResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetCarInfoByID(cx, &pbAdmin.GetCarInfoByIDRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
func (d *Admin) GetAllCarInfosByModelID(ctx *gin.Context, modelID int64) (*pbAdmin.GetAllCarInfoByModelResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllCarInfosByModelID(cx, &pbAdmin.GetAllCarInfoByModelRequest{
		ModelId: modelID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
func (d *Admin) UpdateCarInfo(ctx *gin.Context, input domain.CarInfoUpdateInput) (*pbAdmin.UpdateCarInfoResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.UpdateCarInfo(cx, &pbAdmin.UpdateCarInfoRequest{
		ID:           input.ID,
		ModelId:      input.ModelID,
		Length:       input.Length,
		Width:        input.Width,
		Height:       input.Height,
		Volume:       input.Volume,
		LoadCapacity: input.LoadCapacity,
		Close:        input.Close,
		Description:  input.Description,
		AuthorId:     input.AuthorID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
func (d *Admin) DeleteCarInfo(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCarInfoResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.DeleteCarInfo(cx, &pbAdmin.DeleteCarInfoRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
