package admin

import (
	"context"
	"fmt"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (d *Admin) CreateCity(ctx *gin.Context, input domain.CityCreateInput) (*pbAdmin.CreateCityResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.CreateCity(cx, &pbAdmin.CreateCityRequest{
		Name:      input.Name,
		Status:    fmt.Sprint(input.Status),
		CountryId: input.CountryID,
		AuthorId:  input.AuthorID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetAllCities(ctx *gin.Context) (*pbAdmin.GetAllCitiesResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllCities(cx, &pbAdmin.GetAllCitiesRequest{
		Status: 1,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetAllCitiesByCountryID(ctx *gin.Context, countryID int64) (*pbAdmin.GetAllCitiesByCountryResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllCitiesByCountryID(cx, &pbAdmin.GetAllCitiesByCountryRequest{
		CountryId: countryID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetCityByID(ctx *gin.Context, ID int64) (*pbAdmin.GetCityByIDResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetCityByID(cx, &pbAdmin.GetCityByIDRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) UpdateCity(ctx *gin.Context, input domain.CityUpdateInput) (*pbAdmin.UpdateCityResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.UpdateCity(cx, &pbAdmin.UpdateCityRequest{
		ID:        input.ID,
		Name:      input.Name,
		Status:    fmt.Sprint(input.Status),
		CountryId: input.CountryID,
		AuthorId:  int64(input.AuthorID),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) DeleteCity(ctx *gin.Context, ID int64) (*pbAdmin.DeleteCityResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.DeleteCity(cx, &pbAdmin.DeleteCityRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
