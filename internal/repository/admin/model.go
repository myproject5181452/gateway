package admin

import (
	"context"
	"fmt"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (d *Admin) CreateModel(ctx *gin.Context, input domain.ModelCreateInput) (*pbAdmin.CreateModelResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.CreateModel(cx, &pbAdmin.CreateModelRequest{
		Name:     input.Name,
		Status:   fmt.Sprint(input.Status),
		BrandId:  input.BrandID,
		AuthorId: input.AuthorID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetAllModels(ctx *gin.Context) (*pbAdmin.GetAllModelsResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllModels(cx, &pbAdmin.GetAllModelsRequest{
		Status: 1,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetAllModelsByBrandID(ctx *gin.Context, brandID int64) (*pbAdmin.GetAllModelsByBrandResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetAllModelsByBrandID(cx, &pbAdmin.GetAllModelsByBrandRequest{
		BrandId: brandID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) GetModelByID(ctx *gin.Context, ID int64) (*pbAdmin.GetModelByIDResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.GetModelByID(cx, &pbAdmin.GetModelByIDRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) UpdateModel(ctx *gin.Context, input domain.ModelUpdateInput) (*pbAdmin.UpdateModelResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.UpdateModel(cx, &pbAdmin.UpdateModelRequest{
		ID:       input.ID,
		Name:     input.Name,
		Status:   fmt.Sprint(input.Status),
		BrandId:  input.BrandID,
		AuthorId: int64(input.AuthorID),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}

func (d *Admin) DeleteModel(ctx *gin.Context, ID int64) (*pbAdmin.DeleteModelResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.DeleteModel(cx, &pbAdmin.DeleteModelRequest{
		ID: ID,
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
