package admin

import (
	"context"
	"fmt"
	"time"

	"github.com/akijura/myProject/gateway/internal/config"
	"github.com/akijura/myProject/gateway/internal/entity/domain"
	pbAdmin "github.com/akijura/myProject/gateway/internal/repository/admin/pb"
	"github.com/gin-gonic/gin"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

func (d *Admin) CreateUser(ctx *gin.Context, input domain.UserCreateInput) (*pbAdmin.CreateUserResponse, error) {
	conn, err := grpc.Dial(config.NewConfig().RPCAdminService, grpc.WithTransportCredentials(insecure.NewCredentials()), grpc.WithBlock())
	if err != nil {
		return nil, err
	}

	defer conn.Close()
	c := pbAdmin.NewAdminSerClient(conn)
	cx, cancel := context.WithTimeout(context.Background(), time.Second*10)
	defer cancel()
	rsp, err := c.CreateUser(cx, &pbAdmin.CreateUserRequest{
		Login:       input.Login,
		Password:    input.Password,
		Name:        input.Name,
		PhoneNumber: input.PhoneNumber,
		Role:        int64(input.Role),
		Status:      fmt.Sprint(input.Status),
	})
	if err != nil {
		return nil, err
	}
	return rsp, nil
}
