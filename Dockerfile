FROM golang:1.21.5-bullseye 

WORKDIR /usr/src/gateway


COPY . .
RUN go mod download

# Build
RUN go build -o /main ./cmd/main.go

ARG PROXY

ENV HTTP_PROXY=${PROXY}
ENV HTTPS_PROXY=${PROXY}


RUN export http_proxy=${PROXY} \
    && export https_proxy=${PROXY} \
    && apt-get update -y

EXPOSE 9696
CMD [ "/main" ]

